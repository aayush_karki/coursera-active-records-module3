class TodoItem < ActiveRecord::Base

	def self.count_completed_items
		return TodoItem.where(completed: true).count
	end
end
